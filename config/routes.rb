Rails.application.routes.draw do
  resources :users
  
  match '/signup', to: 'users#new', via: [:get, :post]

    # Static Page Routes
  match '/help',  to: 'static_pages#help', via: [:get, :post]
  match '/about',  to: 'static_pages#about', via: [:get, :post]
  match '/contact',  to: 'static_pages#contact', via: [:get, :post]

  root :to => 'static_pages#home'



end

