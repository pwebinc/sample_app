require 'rails_helper'

describe 'User Pages' do
  subject { page }

  describe 'signup page' do
    before { visit signup_path }

    it { should have_selector('h1', text: 'Sign up') }
    it { should have_title(full_title('Sign up')) }
  end

  describe 'profile page' do
    let(:user) { FactoryBot.create(:user) }

    before { visit user_path(user) }

    it { should have_selector('h1', text: user.name) }
    it { should have_title(full_title(user.name)) }
  end

  describe 'signup' do

    before { visit signup_path }

    let(:submit) { 'Create my account' }

    describe 'with invalid information' do
      it 'should not create a user' do
        old_count = User.count
        click_button 'Create my account'
        new_count = User.count
        new_count.should == old_count
      end

      describe "after submission" do 
        before { click_button submit }
        it { should have_title(full_title('Sign up')) }
        it { should have_content('error') }
        it { should_not have_content('Password digest') }
      end
    end

    describe 'with valid information' do
      it 'should create a user' do
        old_count = User.count
        fill_in 'Name', with: 'Example User'
        fill_in 'Email', with: 'exampley@example.com'
        fill_in 'Password', with: 'foobar'
        fill_in 'Confirmation', with: 'foobar'
        click_button 'Create my account'
        new_count = User.count
        new_count == old_count + 1
      end
    end

      describe "signup page" do
    before { visit signup_path }

    it { should have_content('Sign up') }
    it { should have_title(full_title('Sign up')) }
  end

      describe "after submission" do
        before { click_button submit }

        it { should have_title('Sign up') }
        it { should have_content('error') }
      end
  end
end
